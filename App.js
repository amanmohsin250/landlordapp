import React from "react";
import { StyleSheet, Text, View } from "react-native";
import MainScreen from "./screens/Main";
import LoginScreen from "./screens/Login";
import SignupScreen from "./screens/Signup";
import HomeTabs from "./screens/HomeTabs";
import PhoneVerficationScreen from "./screens/PhoneVerification";
import AddNewTenant from "./screens/AddNewTenant";
import ListOfTenants from "./screens/ListOfTenants";
import OnDemandServices from "./screens/OnDemandServices";
import SubscriptionBasedServices from "./screens/SubscriptionBasedServices";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Main"
        screenOptions={{ headerShown: false }}
      >
        <Stack.Screen name="Main" component={MainScreen} />
        <Stack.Screen name="Login" component={LoginScreen} />
        <Stack.Screen name="Signup" component={SignupScreen} />
        <Stack.Screen
          name="PhoneVerification"
          component={PhoneVerficationScreen}
        />
        <Stack.Screen name="HomeTabs" component={HomeTabs} />
        <Stack.Screen name="AddNewTenant" component={AddNewTenant} />
        <Stack.Screen name="ListOfTenants" component={ListOfTenants} />
        <Stack.Screen name="OnDemandServices" component={OnDemandServices} />
        <Stack.Screen
          name="SubscriptionBasedServices"
          component={SubscriptionBasedServices}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({});
