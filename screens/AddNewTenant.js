import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Keyboard,
} from "react-native";
import PageHeading from "../components/PageHeading";
import Card from "../components/Card";
import {
  Ionicons,
  FontAwesome5,
  Zocial,
  FontAwesome,
  Fontisto,
} from "@expo/vector-icons";
import db from "../server/db/firestore";

export default function AddNewTenant({ navigation }) {
  const [enteredFirstName, setEnteredFirstName] = useState("");
  const [enteredLastName, setEnteredLastName] = useState("");
  const [enteredEmail, setEnteredEmail] = useState("");
  const [enteredPhone, setEnteredPhone] = useState("");
  const [enteredAssignedPlot, setEnteredAssignedPlot] = useState("");
  const [enteredMovingDate, setEnteredMovingDate] = useState("");
  const [enteredPaymentMethod, setEnteredPaymentMethod] = useState("");

  const AddTenantHandler = () => {
    db.collection("tenants")
      .add({
        FirstName: enteredFirstName,
        LastName: enteredLastName,
        Email: enteredEmail,
        Phone: enteredPhone,
        AssignedPlot: enteredAssignedPlot,
        MovingDate: enteredMovingDate,
        PaymentMethod: enteredPaymentMethod,
      })
      .then((result) => navigation.goBack())
      .catch((err) => console.log(err));
  };

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <View style={styles.container}>
        <PageHeading title="Add New Tenant" />

        <Card>
          <Card List>
            <View style={styles.cardTextInputContainer}>
              <Ionicons name="person" size={24} color="#FB6E19" />
              <TextInput
                placeholder="First Name"
                placeholderTextColor="black"
                style={{ width: "80%" }}
                value={enteredFirstName}
                onChangeText={(text) => setEnteredFirstName(text)}
              />
            </View>
          </Card>
          <Card List>
            <View style={styles.cardTextInputContainer}>
              <Ionicons name="person" size={24} color="#FB6E19" />
              <TextInput
                placeholder="Last Name"
                placeholderTextColor="black"
                style={{ width: "80%" }}
                value={enteredLastName}
                onChangeText={(text) => setEnteredLastName(text)}
              />
            </View>
          </Card>
          <Card List>
            <View style={styles.cardTextInputContainer}>
              <FontAwesome5 name="phone-alt" size={24} color="#FB6E19" />
              <TextInput
                placeholder="Phone #"
                placeholderTextColor="black"
                style={{ width: "80%" }}
                keyboardType="phone-pad"
                value={enteredPhone}
                onChangeText={(text) => setEnteredPhone(text)}
              />
            </View>
          </Card>
          <Card List>
            <View style={styles.cardTextInputContainer}>
              <Zocial name="email" size={24} color="#FB6E19" />
              <TextInput
                placeholder="Email"
                placeholderTextColor="black"
                style={{ width: "80%" }}
                keyboardType="email-address"
                value={enteredEmail}
                onChangeText={(text) => setEnteredEmail(text)}
              />
            </View>
          </Card>
          <Card List>
            <View style={styles.cardTextInputContainer}>
              <FontAwesome name="building" size={24} color="#FB6E19" />
              <TextInput
                placeholder="Assigned Plot/Villa"
                placeholderTextColor="black"
                style={{ width: "80%" }}
                value={enteredAssignedPlot}
                onChangeText={(text) => setEnteredAssignedPlot(text)}
              />
            </View>
          </Card>
          <Card List>
            <View style={styles.cardTextInputContainer}>
              <FontAwesome5 name="calendar-alt" size={24} color="#FB6E19" />
              <TextInput
                placeholder="Moving Date"
                placeholderTextColor="black"
                style={{ width: "80%" }}
                value={enteredMovingDate}
                onChangeText={(text) => setEnteredMovingDate(text)}
              />
            </View>
          </Card>
          <Card List>
            <TextInput
              placeholder="Payment Methods"
              placeholderTextColor="black"
              style={{ width: "100%" }}
              value={enteredPaymentMethod}
              onChangeText={(text) => setEnteredPaymentMethod(text)}
            />
            <View style={styles.cardTextInputContainer}>
              <FontAwesome5 name="stripe" size={28} color="#FB6E19" />
              <Fontisto name="paypal" size={28} color="#FB6E19" />
              <FontAwesome5 name="cc-mastercard" size={28} color="#FB6E19" />
              <FontAwesome5 name="cc-visa" size={28} color="#FB6E19" />
            </View>
          </Card>
        </Card>

        <View style={styles.bottom}>
          <TouchableOpacity activeOpacity={0.6} onPress={AddTenantHandler}>
            <View style={styles.button}>
              <Text
                style={{
                  textAlign: "center",
                  fontSize: 16,
                  fontWeight: "bold",
                  color: "white",
                }}
              >
                Add Tenant
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  cardTextInputContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  bottom: {
    flex: 1,
    justifyContent: "flex-end",
    marginBottom: 30,
  },
  button: {
    marginHorizontal: "10%",
    backgroundColor: "#FB6E19",
    borderRadius: 10,
    paddingVertical: 15,
  },
});
