import React from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
} from "react-native";
import PageHeading from "../components/PageHeading";

export default function Services({ navigation }) {
  return (
    <View style={styles.container}>
      <PageHeading title="Services" />

      <View style={styles.topBoxesContainer}>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          <TouchableOpacity style={styles.activeTopBox} activeOpacity={0.8}>
            <View style={{ alignItems: "center" }}>
              <Image
                source={require("../assets/images/Icons/ICON-NO-3.png")}
                style={styles.topBoxImage}
              />
              <Text style={styles.topBoxText}>Services Booked</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.topBox}
            activeOpacity={0.8}
            onPress={() => navigation.navigate("OnDemandServices")}
          >
            <View style={{ alignItems: "center" }}>
              <Image
                source={require("../assets/images/Icons/icon-no-8.png")}
                style={styles.topBoxImage}
              />
              <Text style={styles.topBoxText}>On Demand Services</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.topBox}
            activeOpacity={0.8}
            onPress={() => navigation.navigate("SubscriptionBasedServices")}
          >
            <View style={{ alignItems: "center" }}>
              <Image
                source={require("../assets/images/Icons/icon-no-9.png")}
                style={styles.topBoxImage}
              />
              <Text style={styles.topBoxText}>Subscription Based Services</Text>
            </View>
          </TouchableOpacity>
        </ScrollView>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  topBoxesContainer: {
    marginBottom: 30,
  },
  activeTopBox: {
    backgroundColor: "#F16714",
    paddingVertical: 15,
    borderRadius: 20,
    width: "30%",
    alignItems: "center",
    marginHorizontal: 10,
  },
  topBox: {
    backgroundColor: "#3BBFFC",
    paddingVertical: 15,
    borderRadius: 20,
    width: "30%",
    alignItems: "center",
    marginHorizontal: 10,
  },
  topBoxImage: {
    height: 50,
    width: 50,
    resizeMode: "contain",
    marginBottom: 10,
  },
  topBoxText: {
    color: "white",
    fontSize: 15,
    textAlign: "center",
    marginHorizontal: 20,
  },
});
