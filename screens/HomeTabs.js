import * as React from "react";
import { Text, View, Image } from "react-native";
import Home from "./Home";
import Services from "./Services";
import Tenants from "./Tenants";
import Tickets from "./Tickets";
import Chat from "./Chat";
import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { MaterialIcons, Entypo } from "@expo/vector-icons";

const Tab = createBottomTabNavigator();

export default function HomeTabs() {
  return (
    <Tab.Navigator
      initialRouteName="Dashboard"
      screenOptions={({ route }) => ({
        headerShown: false,
        tabBarActiveTintColor: "#C54A00",
        tabBarInactiveTintColor: "#FB6E19",
        tabBarIcon: ({ color }) => {
          if (route.name == "Dashboard") {
            return <MaterialIcons name="dashboard" size={24} color={color} />;
          } else if (route.name == "Services") {
            return (
              <Image
                source={require("../assets/images/Icons/ICON-NO-3.png")}
                style={{ width: 30, height: 30, tintColor: color }}
              />
            );
          } else if (route.name == "Chat") {
            return <Entypo name="chat" size={24} color={color} />;
          } else if (route.name == "Tenants") {
            return (
              <Image
                source={require("../assets/images/Icons/Icon-No-1.png")}
                style={{ width: 30, height: 30, tintColor: color }}
              />
            );
          } else if (route.name == "Tickets") {
            return <Entypo name="ticket" size={24} color={color} />;
          }
        },
      })}
    >
      <Tab.Screen name="Dashboard" component={Home} />
      <Tab.Screen name="Services" component={Services} />
      <Tab.Screen name="Tenants" component={Tenants} />
      <Tab.Screen name="Tickets" component={Tickets} />
      <Tab.Screen name="Chat" component={Chat} />
    </Tab.Navigator>
  );
}
