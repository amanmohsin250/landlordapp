import React from "react";
import { View, Text, StyleSheet } from "react-native";
import PageHeading from "../components/PageHeading";

export default function Tickets() {
  return (
    <View style={styles.container}>
      <PageHeading title="Tickets" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
});
