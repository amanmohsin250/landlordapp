import React from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image } from "react-native";
import { LinearGradient } from "expo-linear-gradient";

export default function Main({ navigation }) {
  return (
    <View style={styles.container}>
      <LinearGradient
        // Background Linear Gradient
        colors={["#3BBFFC", "transparent"]}
        style={styles.background}
      />
      <View style={styles.bottom}>
        <View style={{ alignItems: "center", marginVertical: "35%" }}>
          <Image
            source={require("../assets/images/app-logo-White.png")}
            style={{ height: 100, width: "80%" }}
          />
        </View>
        <TouchableOpacity onPress={() => navigation.replace("Login")}>
          <View style={styles.LandLordButton}>
            <Text
              style={{
                color: "white",
                textAlign: "center",
                fontWeight: "bold",
              }}
            >
              LandLord
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity>
          <View style={styles.TenantButton}>
            <Text
              style={{
                color: "white",
                textAlign: "center",
                fontWeight: "bold",
              }}
            >
              Tenant
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#005FF2",
  },
  bottom: {
    flex: 1,
    justifyContent: "flex-end",
    marginBottom: "20%",
  },
  background: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    height: "100%",
  },
  LandLordButton: {
    backgroundColor: "#000000",
    paddingVertical: 15,
    width: "60%",
    marginHorizontal: "20%",
    borderRadius: 25,
    marginVertical: 15,
  },
  TenantButton: {
    backgroundColor: "#FB6E19",
    paddingVertical: 15,
    width: "60%",
    marginHorizontal: "20%",
    borderRadius: 25,
    marginVertical: 15,
  },
});
