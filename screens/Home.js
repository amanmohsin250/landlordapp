import React from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  TouchableWithoutFeedback,
  Keyboard,
  Image,
  ScrollView,
  TouchableOpacityBase,
} from "react-native";
import { Avatar, Card } from "react-native-paper";
import {
  Ionicons,
  AntDesign,
  Fontisto,
  SimpleLineIcons,
} from "@expo/vector-icons";
import CircularCard from "../components/CircularCard";

export default function Home() {
  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <View>
            <Avatar.Image
              size={50}
              source={require("../assets/images/Avatar.png")}
            />
          </View>
          <View style={{ width: "60%" }}>
            <Text>Welcome Landlord!</Text>
            <Text style={{ fontWeight: "bold" }}>Aman Mohsin</Text>
          </View>
          <View>
            <TouchableOpacity>
              <CircularCard>
                <Ionicons
                  name="notifications-outline"
                  size={24}
                  color="black"
                />
              </CircularCard>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.SearchContainer}>
          <View style={styles.SearchBox}>
            <AntDesign name="search1" size={24} color="black" />
            <TextInput
              placeholder="Search"
              style={{ paddingLeft: 10, width: "90%" }}
            />
          </View>
          <View>
            <Fontisto name="equalizer" size={24} color="black" />
          </View>
        </View>
        <ScrollView>
          <View style={styles.topBoxesContainer}>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
            >
              <TouchableOpacity style={styles.topBox} activeOpacity={0.8}>
                <View style={{ alignItems: "center" }}>
                  <Image
                    source={require("../assets/images/Icons/Icon-No-1.png")}
                    style={{
                      height: 50,
                      width: 50,
                      resizeMode: "contain",
                      marginBottom: 10,
                    }}
                  />
                  <Text
                    style={{
                      color: "white",
                      fontSize: 15,
                      textAlign: "center",
                      marginHorizontal: 20,
                    }}
                  >
                    Total
                  </Text>
                  <Text
                    style={{
                      color: "white",
                      fontSize: 15,
                      textAlign: "center",
                      marginHorizontal: 20,
                    }}
                  >
                    Tenants
                  </Text>
                  <Text
                    style={{ color: "white", fontWeight: "bold", fontSize: 20 }}
                  >
                    1500
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={styles.topBox} activeOpacity={0.8}>
                <View style={{ alignItems: "center" }}>
                  <Image
                    source={require("../assets/images/Icons/Icon-no-2.png")}
                    style={{
                      height: 50,
                      width: 50,
                      resizeMode: "contain",
                      marginBottom: 10,
                    }}
                  />
                  <Text
                    style={{
                      color: "white",
                      fontSize: 15,
                      textAlign: "center",
                      marginHorizontal: 20,
                    }}
                  >
                    Remaining Tenant Dues
                  </Text>
                  <Text
                    style={{ color: "white", fontWeight: "bold", fontSize: 20 }}
                  >
                    25,000
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={styles.topBox} activeOpacity={0.8}>
                <View style={{ alignItems: "center" }}>
                  <Image
                    source={require("../assets/images/Icons/ICON-NO-3.png")}
                    style={{
                      height: 50,
                      width: 50,
                      resizeMode: "contain",
                      marginBottom: 10,
                    }}
                  />
                  <Text
                    style={{
                      color: "white",
                      fontSize: 15,
                      textAlign: "center",
                      marginHorizontal: 20,
                    }}
                  >
                    Number of Services
                  </Text>
                  <Text
                    style={{ color: "white", fontWeight: "bold", fontSize: 20 }}
                  >
                    60+
                  </Text>
                </View>
              </TouchableOpacity>
            </ScrollView>
          </View>
          <View style={styles.homeHeadingsContainer}>
            <Text style={{ fontSize: 20, fontWeight: "bold" }}>
              Empty Villas
            </Text>
            <SimpleLineIcons name="options" size={24} color="black" />
          </View>
          <View style={styles.cardContainer}>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
            >
              <TouchableOpacity activeOpacity={0.8}>
                <Card style={styles.apartmentCard}>
                  <Card.Cover
                    source={require("../assets/images/modern_villa.jpg")}
                    style={styles.apartmentsImage}
                  />
                  <Card.Content>
                    <Text style={{ fontSize: 20, fontWeight: "bold" }}>
                      Modern
                    </Text>
                    <Text>Card content</Text>
                  </Card.Content>
                </Card>
              </TouchableOpacity>
              <TouchableOpacity activeOpacity={0.8}>
                <Card style={styles.apartmentCard}>
                  <Card.Cover
                    source={require("../assets/images/classic_villa.jpg")}
                    style={styles.apartmentsImage}
                  />
                  <Card.Content>
                    <Text style={{ fontSize: 20, fontWeight: "bold" }}>
                      Classic
                    </Text>
                    <Text>Card content</Text>
                  </Card.Content>
                </Card>
              </TouchableOpacity>
            </ScrollView>
          </View>
          <View style={styles.homeHeadingsContainer}>
            <Text style={{ fontSize: 20, fontWeight: "bold" }}>
              Empty Apartments
            </Text>
            <SimpleLineIcons name="options" size={24} color="black" />
          </View>
          <View style={styles.cardContainer}>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
            >
              <TouchableOpacity activeOpacity={0.8}>
                <Card style={styles.apartmentCard}>
                  <Card.Cover
                    source={require("../assets/images/modern_apartment.jpg")}
                    style={styles.apartmentsImage}
                  />
                  <Card.Content>
                    <Text style={{ fontSize: 20, fontWeight: "bold" }}>
                      Modern
                    </Text>
                    <Text>Card content</Text>
                  </Card.Content>
                </Card>
              </TouchableOpacity>
              <TouchableOpacity activeOpacity={0.8}>
                <Card style={styles.apartmentCard}>
                  <Card.Cover
                    source={require("../assets/images/classic_apartment.jpg")}
                    style={styles.apartmentsImage}
                  />
                  <Card.Content>
                    <Text style={{ fontSize: 20, fontWeight: "bold" }}>
                      Classic
                    </Text>
                    <Text>Card content</Text>
                  </Card.Content>
                </Card>
              </TouchableOpacity>
            </ScrollView>
          </View>
        </ScrollView>
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  headerContainer: {
    marginTop: "15%",
    marginHorizontal: "10%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 30,
  },
  SearchContainer: {
    flexDirection: "row",
    marginHorizontal: "10%",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 30,
  },
  SearchBox: {
    flexDirection: "row",
    alignItems: "center",
    width: "75%",
    padding: 10,
    backgroundColor: "#F4F4F4",
    borderRadius: 15,
  },
  topBoxesContainer: {
    marginBottom: 30,
  },
  topBox: {
    backgroundColor: "#3BBFFC",
    paddingVertical: 15,
    borderRadius: 20,
    width: "30%",
    alignItems: "center",
    marginHorizontal: 10,
  },
  homeHeadingsContainer: {
    marginHorizontal: "10%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 30,
  },
  cardContainer: {
    marginBottom: 30,
  },
  apartmentCard: {
    marginHorizontal: 10,
    borderRadius: 20,
    width: 250,
  },
  apartmentsImage: {
    margin: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
});
