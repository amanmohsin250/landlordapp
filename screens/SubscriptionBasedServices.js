import React from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
} from "react-native";
import PageHeading from "../components/PageHeading";
import Card from "../components/Card";
import {
  MaterialCommunityIcons,
  MaterialIcons,
  Ionicons,
} from "@expo/vector-icons";

export default function SubscriptionBasedServices({ navigation }) {
  return (
    <View style={styles.container}>
      <PageHeading title="Services" />

      <View style={styles.topBoxesContainer}>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          <TouchableOpacity
            style={styles.topBox}
            activeOpacity={0.8}
            onPress={() => navigation.navigate("Services")}
          >
            <View style={{ alignItems: "center" }}>
              <Image
                source={require("../assets/images/Icons/ICON-NO-3.png")}
                style={styles.topBoxImage}
              />
              <Text style={styles.topBoxText}>Services Booked</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.topBox}
            activeOpacity={0.8}
            onPress={() => navigation.navigate("OnDemandServices")}
          >
            <View style={{ alignItems: "center" }}>
              <Image
                source={require("../assets/images/Icons/icon-no-8.png")}
                style={styles.topBoxImage}
              />
              <Text style={styles.topBoxText}>On Demand Services</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.activeTopBox} activeOpacity={0.8}>
            <View style={{ alignItems: "center" }}>
              <Image
                source={require("../assets/images/Icons/icon-no-9.png")}
                style={styles.topBoxImage}
              />
              <Text style={styles.topBoxText}>Subscription Based Services</Text>
            </View>
          </TouchableOpacity>
        </ScrollView>
      </View>

      <View style={styles.headingContainer}>
        <View style={styles.headingTitle}>
          <Text style={{ fontSize: 20, fontWeight: "bold" }}>
            Subscription Based Services
          </Text>
        </View>
      </View>

      <ScrollView style={styles.ListContainer}>
        {/* 1st Card */}
        <TouchableOpacity activeOpacity={0.6}>
          <Card List>
            <View style={styles.itemContainer}>
              <View style={styles.ItemImageContainer}>
                <Ionicons name="wifi" size={24} color="white" />
              </View>
              <View style={{ width: "40%" }}>
                <Text style={styles.ListTitle}>Internet</Text>
                <Text>Per Day/Hr</Text>
              </View>
              <View>
                <Text style={{ color: "#3BBFFC", fontSize: 12 }}>
                  Starting From
                </Text>
                <Text
                  style={{ color: "#3BBFFC", fontSize: 18, fontWeight: "bold" }}
                >
                  20 AED
                </Text>
              </View>
            </View>
          </Card>
        </TouchableOpacity>
        {/* 2nd Card */}
        <TouchableOpacity activeOpacity={0.6}>
          <Card List>
            <View style={styles.itemContainer}>
              <View style={styles.ItemImageContainer}>
                <MaterialCommunityIcons
                  name="cable-data"
                  size={24}
                  color="white"
                />
              </View>
              <View style={{ width: "40%" }}>
                <Text style={styles.ListTitle}>Cable</Text>
                <Text>Per Day/Hr</Text>
              </View>
              <View>
                <Text style={{ color: "#3BBFFC", fontSize: 12 }}>
                  Starting From
                </Text>
                <Text
                  style={{ color: "#3BBFFC", fontSize: 18, fontWeight: "bold" }}
                >
                  20 AED
                </Text>
              </View>
            </View>
          </Card>
        </TouchableOpacity>
        {/* 3rd Card */}
        <TouchableOpacity activeOpacity={0.6}>
          <Card List>
            <View style={styles.itemContainer}>
              <View style={styles.ItemImageContainer}>
                <MaterialIcons
                  name="electrical-services"
                  size={24}
                  color="white"
                />
              </View>
              <View style={{ width: "40%" }}>
                <Text style={styles.ListTitle}>Electricity</Text>
                <Text>Per Day/Hr</Text>
              </View>
              <View>
                <Text style={{ color: "#3BBFFC", fontSize: 12 }}>
                  Starting From
                </Text>
                <Text
                  style={{ color: "#3BBFFC", fontSize: 18, fontWeight: "bold" }}
                >
                  20 AED
                </Text>
              </View>
            </View>
          </Card>
        </TouchableOpacity>
        {/* 4th Card */}
        <TouchableOpacity activeOpacity={0.6}>
          <Card List>
            <View style={styles.itemContainer}>
              <View style={styles.ItemImageContainer}>
                <Ionicons name="water" size={24} color="white" />
              </View>
              <View style={{ width: "40%" }}>
                <Text style={styles.ListTitle}>Water</Text>
                <Text>Per Day/Hr</Text>
              </View>
              <View>
                <Text style={{ color: "#3BBFFC", fontSize: 12 }}>
                  Starting From
                </Text>
                <Text
                  style={{ color: "#3BBFFC", fontSize: 18, fontWeight: "bold" }}
                >
                  20 AED
                </Text>
              </View>
            </View>
          </Card>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  topBoxesContainer: {
    marginBottom: 30,
  },
  activeTopBox: {
    backgroundColor: "#F16714",
    paddingVertical: 15,
    borderRadius: 20,
    width: "30%",
    alignItems: "center",
    marginHorizontal: 10,
  },
  topBox: {
    backgroundColor: "#3BBFFC",
    paddingVertical: 15,
    borderRadius: 20,
    width: "30%",
    alignItems: "center",
    marginHorizontal: 10,
  },
  topBoxImage: {
    height: 50,
    width: 50,
    resizeMode: "contain",
    marginBottom: 10,
  },
  topBoxText: {
    color: "white",
    fontSize: 15,
    textAlign: "center",
    marginHorizontal: 20,
  },
  headingTitle: {
    paddingVertical: 10,
    borderBottomWidth: 2,
    borderBottomColor: "#005FF2",
  },
  headingContainer: {
    marginBottom: 30,
    marginHorizontal: "10%",
    flexDirection: "row",
  },
  ListContainer: {
    marginHorizontal: "10%",
  },
  ListTitle: {
    fontSize: 18,
    fontWeight: "bold",
    marginVertical: 5,
  },
  ItemImageContainer: {
    backgroundColor: "#F16714",
    padding: 10,
    borderRadius: 50,
  },
  itemContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
});
