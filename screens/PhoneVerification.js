import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TextInput,
  Keyboard,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from "react-native";
import firebase from "../server/db/firestore";

export default function PhoneVerification({ navigation, route }) {
  const [code, setCode] = useState("");
  const { verificationId } = route.params;
  const LoginHandler = () => {
    // navigation.replace("HomeTabs");
    const credential = firebase.auth.PhoneAuthProvider.credential(
      verificationId,
      code
    );
    firebase
      .auth()
      .signInWithCredential(credential)
      .then((result) => {
        // Do something with the results here
        console.log(result);
      });
  };
  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <View style={styles.container}>
        <View style={styles.logoContainer}>
          <Image
            source={require("../assets/images/app-Coloured-Logo.png")}
            style={{ height: 100, width: "50%" }}
          />
        </View>
        <View style={styles.LoginFormContainer}>
          <Text style={styles.LoginHeading}>Phone Verification</Text>
          <Text style={styles.detailsText}>Enter your OTP code here</Text>
          <View style={styles.OTPTextInputContainer}>
            {/* <View style={styles.OTPTextInput}>
              <TextInput
                maxLength={1}
                keyboardType="number-pad"
                style={{ color: "white", width: "100%" }}
              />
            </View>
            <View style={styles.OTPTextInput}>
              <TextInput
                maxLength={1}
                keyboardType="number-pad"
                style={{ color: "white", width: "100%" }}
              />
            </View>
            <View style={styles.OTPTextInput}>
              <TextInput
                maxLength={1}
                keyboardType="number-pad"
                style={{ color: "white", width: "100%" }}
              />
            </View>
            <View style={styles.OTPTextInput}>
              <TextInput
                maxLength={1}
                keyboardType="number-pad"
                style={{ color: "white", width: "100%" }}
              />
            </View> */}
            <View style={styles.OTPTextInput}>
              <TextInput
                maxLength={6}
                keyboardType="number-pad"
                style={{ color: "white", width: "100%", textAlign: "center" }}
                onChangeText={(enteredText) => setCode(enteredText)}
              />
            </View>
          </View>
          <TouchableOpacity
            style={styles.ResendOTPContainer}
            activeOpacity={0.8}
          >
            <Text style={{ color: "white" }}>Resend OTP</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={LoginHandler}>
            <View style={styles.LoginButton}>
              <Text style={{ color: "white", textAlign: "center" }}>Login</Text>
            </View>
          </TouchableOpacity>
          <View
            style={{
              marginHorizontal: "10%",
              flexDirection: "row",
              justifyContent: "center",
            }}
          >
            <Text style={{ color: "white" }}>Don't have account? </Text>
            <TouchableOpacity onPress={() => navigation.navigate("Signup")}>
              <Text style={{ color: "#3AB5FA" }}>Sign up</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  logoContainer: {
    marginVertical: "5%",
    alignItems: "center",
  },
  LoginFormContainer: {
    backgroundColor: "#000000",
    width: "90%",
    marginHorizontal: "5%",
    paddingVertical: "15%",
    borderRadius: 25,
  },
  LoginHeading: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
    fontSize: 30,
    marginBottom: 15,
  },
  detailsText: {
    color: "#BABABA",
    textAlign: "center",
    marginHorizontal: "10%",
    marginBottom: "10%",
  },
  inputFieldContainer: {
    marginHorizontal: "10%",
    flexDirection: "row",
    paddingVertical: 5,
    borderBottomWidth: 1,
    borderBottomColor: "#BABABA",
    marginVertical: 20,
  },
  inputStyle: {
    width: "90%",
    marginLeft: 5,
    color: "white",
  },
  ForgotPasswordContainer: {
    marginHorizontal: "10%",
    flexDirection: "row",
    justifyContent: "flex-end",
    marginBottom: 20,
  },
  LoginButton: {
    backgroundColor: "#FB6E19",
    marginHorizontal: "10%",
    paddingVertical: 15,
    borderRadius: 10,
    marginBottom: 20,
  },
  GoogleButton: {
    backgroundColor: "white",
    width: "45%",
    padding: 10,
    borderRadius: 25,
    flexDirection: "row",
    alignItems: "center",
  },
  FacebookButton: {
    backgroundColor: "#245A9C",
    width: "45%",
    padding: 10,
    borderRadius: 25,
    flexDirection: "row",
    alignItems: "center",
    marginLeft: "10%",
  },
  SocialButtons: {
    marginHorizontal: "10%",
    flexDirection: "row",
  },
  ResendOTPContainer: {
    marginHorizontal: "10%",
    flexDirection: "row",
    justifyContent: "flex-end",
    marginBottom: 20,
  },
  OTPTextInput: {
    padding: 5,
    width: "100%",
    borderBottomWidth: 2,
    borderBottomColor: "white",
  },
  OTPTextInputContainer: {
    marginHorizontal: "10%",
    //flexDirection: "row",
    marginBottom: 20,
    //justifyContent: "space-between",
  },
});
