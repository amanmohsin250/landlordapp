import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TextInput,
  Keyboard,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from "react-native";
import { MaterialIcons, EvilIcons } from "@expo/vector-icons";
import { db } from "../server/db/firestore";

export default function Login({ navigation }) {
  const [users, setUsers] = useState();

  const LoginHandler = () => {
    navigation.replace("HomeTabs");
    console.log(users);
  };

  useEffect(() => {
    db.collection("users").onSnapshot({
      next: (querySnapshot) => {
        const users = querySnapshot.docs.map((docSnapshot) => ({
          Email: docSnapshot.data().Email,
          Password: docSnapshot.data().Password,
        }));
        setUsers(users);
      },
      error: (err) => console.log(err),
    });
    db.collection("users")
      .get()
      .then((result) => result.docs)
      .then((docs) =>
        docs.map((doc) => ({
          Email: doc.data().Email,
          Password: doc.data().Password,
        }))
      )
      .then((users) => setUsers(users));
  }, [setUsers]);
  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <View style={styles.container}>
        <View style={styles.logoContainer}>
          <Image
            source={require("../assets/images/app-Coloured-Logo.png")}
            style={{ height: 100, width: "50%" }}
          />
        </View>
        <View style={styles.LoginFormContainer}>
          <Text style={styles.LoginHeading}>Login</Text>
          <Text style={styles.detailsText}>
            Please enter your email & phone number to Login
          </Text>
          <View style={styles.inputFieldContainer}>
            <MaterialIcons name="email" size={24} color="white" />
            <TextInput
              placeholder="Email or phone"
              placeholderTextColor="white"
              style={styles.inputStyle}
            />
          </View>
          <View style={styles.inputFieldContainer}>
            <EvilIcons name="unlock" size={28} color="white" />
            <TextInput
              placeholder="Password"
              placeholderTextColor="white"
              style={styles.inputStyle}
              secureTextEntry={true}
            />
          </View>
          <TouchableOpacity
            style={styles.ForgotPasswordContainer}
            activeOpacity={0.8}
          >
            <Text style={{ color: "white" }}>Forgot Password</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={LoginHandler}>
            <View style={styles.LoginButton}>
              <Text style={{ color: "white", textAlign: "center" }}>Login</Text>
            </View>
          </TouchableOpacity>
          <View
            style={{
              marginHorizontal: "10%",
              flexDirection: "row",
              justifyContent: "center",
            }}
          >
            <Text style={{ color: "white" }}>Don't have account? </Text>
            <TouchableOpacity onPress={() => navigation.replace("Signup")}>
              <Text style={{ color: "#3AB5FA" }}>Sign up</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  logoContainer: {
    marginVertical: "5%",
    alignItems: "center",
  },
  LoginFormContainer: {
    backgroundColor: "#000000",
    width: "90%",
    marginHorizontal: "5%",
    paddingVertical: "15%",
    borderRadius: 25,
  },
  LoginHeading: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
    fontSize: 30,
    marginBottom: 15,
  },
  detailsText: {
    color: "#BABABA",
    textAlign: "center",
    marginHorizontal: "10%",
    marginBottom: "10%",
  },
  inputFieldContainer: {
    marginHorizontal: "10%",
    flexDirection: "row",
    paddingVertical: 5,
    borderBottomWidth: 1,
    borderBottomColor: "#BABABA",
    marginVertical: 20,
  },
  inputStyle: {
    width: "90%",
    marginLeft: 5,
    color: "white",
  },
  ForgotPasswordContainer: {
    marginHorizontal: "10%",
    flexDirection: "row",
    justifyContent: "flex-end",
    marginBottom: 20,
  },
  LoginButton: {
    backgroundColor: "#FB6E19",
    marginHorizontal: "10%",
    paddingVertical: 15,
    borderRadius: 10,
    marginBottom: 20,
  },
});
