import React, { useState } from "react";
import { View, Text, StyleSheet, ScrollView } from "react-native";
import PageHeading from "../components/PageHeading";
import Card from "../components/Card";
import TopTabs from "../components/TopTabs";
import { Entypo, FontAwesome5, FontAwesome } from "@expo/vector-icons";
import { Avatar } from "react-native-paper";
import db from "../server/db/firestore";

export default function ListOfTenants() {
  const [tenantsList, setTenantsList] = useState();

  React.useEffect(() => {
    db.collection("tenants").onSnapshot({
      next: (querySnapshot) => {
        const tenants = querySnapshot.docs.map((docSnapshot) => ({
          Name:
            docSnapshot.data().FirstName + " " + docSnapshot.data().LastName,
          Since: docSnapshot.data().MovingDate,
          Apartment: docSnapshot.data().AssignedPlot,
        }));
        setTenantsList(tenants);
      },
      error: (err) => console.log(err),
    });

    db.collection("tenants")
      .get()
      .then((result) => result.docs)
      .then((docs) =>
        docs.map((doc) => ({
          Name: doc.data().FirstName + " " + doc.data().LastName,
          Since: doc.data().MovingDate,
          Apartment: doc.data().AssignedPlot,
        }))
      )
      .then((tenants) => setTenantsList(tenants))
      .catch((err) => console.log(err));
  }, [setTenantsList]);

  const List = [
    {
      Name: "Dillion Smith",
      Since: "2020",
      Apartment: 1254,
    },
    {
      Name: "Dre Yung",
      Since: "2020",
      Apartment: 1203,
    },
    {
      Name: "Duke",
      Since: "2020",
      Apartment: 1254,
    },
    {
      Name: "Duke Jones",
      Since: "2020",
      Apartment: 1254,
    },
    {
      Name: "Eris Won",
      Since: "2020",
      Apartment: 1254,
    },
  ];
  return (
    <View style={styles.container}>
      <PageHeading title="List Of Tenants" />

      <Card>
        <TopTabs first="All" second="New" third="Old" />
        <ScrollView showsVerticalScrollIndicator={false}>
          {tenantsList &&
            tenantsList.map((item, index) => (
              <View key={index}>
                <Card List>
                  <View style={styles.ListInnerContainer}>
                    <View>
                      <Avatar.Image
                        size={30}
                        source={require("../assets/images/Avatar.png")}
                      />
                    </View>
                    <View style={{ width: "65%" }}>
                      <View>
                        <Text>{item.Name}</Text>
                      </View>
                      <View style={{ flexDirection: "row", marginTop: 5 }}>
                        <View
                          style={{
                            flexDirection: "row",
                            alignItems: "center",
                            marginRight: 5,
                          }}
                        >
                          <FontAwesome5
                            name="calendar-alt"
                            size={12}
                            color="#3BBFFC"
                          />
                          <Text style={{ color: "#3BBFFC", fontSize: 10 }}>
                            Since {item.Since}
                          </Text>
                        </View>
                        <View
                          style={{ flexDirection: "row", alignItems: "center" }}
                        >
                          <FontAwesome
                            name="building"
                            size={12}
                            color="#C54A00"
                          />
                          <Text style={{ color: "#C54A00", fontSize: 10 }}>
                            Apartment # {item.Apartment}
                          </Text>
                        </View>
                      </View>
                    </View>
                    <Entypo
                      name="dots-three-vertical"
                      size={14}
                      color="black"
                    />
                  </View>
                </Card>
              </View>
            ))}
        </ScrollView>
      </Card>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  ListInnerContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
});
