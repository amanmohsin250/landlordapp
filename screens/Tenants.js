import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
} from "react-native";
import PageHeading from "../components/PageHeading";
import Card from "../components/Card";
import TopTabs from "../components/TopTabs";
import { Entypo, FontAwesome5, FontAwesome } from "@expo/vector-icons";
import { Avatar } from "react-native-paper";
import db from "../server/db/firestore";

export default function Tenants({ navigation }) {
  const [tenantsList, setTenantsList] = useState();

  React.useEffect(() => {
    db.collection("tenants").onSnapshot({
      next: (querySnapshot) => {
        const tenants = querySnapshot.docs.map((docSnapshot) => ({
          Name:
            docSnapshot.data().FirstName + " " + docSnapshot.data().LastName,
          Since: docSnapshot.data().MovingDate,
          Apartment: docSnapshot.data().AssignedPlot,
        }));
        setTenantsList(tenants);
      },
      error: (err) => console.log(err),
    });

    db.collection("tenants")
      .get()
      .then((result) => result.docs)
      .then((docs) =>
        docs.map((doc) => ({
          Name: doc.data().FirstName + " " + doc.data().LastName,
          Since: doc.data().MovingDate,
          Apartment: doc.data().AssignedPlot,
        }))
      )
      .then((tenants) => setTenantsList(tenants))
      .catch((err) => console.log(err));
  }, [setTenantsList]);

  return (
    <View style={styles.container}>
      <PageHeading title="Tenants" />

      <View style={styles.topBoxesContainer}>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          <TouchableOpacity
            style={styles.topBox}
            activeOpacity={0.8}
            onPress={() => navigation.navigate("ListOfTenants")}
          >
            <View style={{ alignItems: "center" }}>
              <Image
                source={require("../assets/images/Icons/icon-no-4.png")}
                style={styles.topBoxImage}
              />
              <Text style={styles.topBoxText}>List of Tenants</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.topBox}
            activeOpacity={0.8}
            onPress={() => navigation.navigate("AddNewTenant")}
          >
            <View style={{ alignItems: "center" }}>
              <Image
                source={require("../assets/images/Icons/icon-no-5.png")}
                style={styles.topBoxImage}
              />
              <Text style={styles.topBoxText}>Add New Tenant</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.topBox} activeOpacity={0.8}>
            <View style={{ alignItems: "center" }}>
              <Image
                source={require("../assets/images/Icons/icon-no-6.png")}
                style={styles.topBoxImage}
              />
              <Text style={styles.topBoxText}>Tenant Pending Dues</Text>
            </View>
          </TouchableOpacity>
        </ScrollView>
      </View>

      <Card>
        <TopTabs first="All" second="New" third="Old" />
        <ScrollView showsVerticalScrollIndicator={false}>
          {tenantsList &&
            tenantsList.map((item, index) => (
              <View key={index}>
                <Card List>
                  <View style={styles.ListInnerContainer}>
                    <View>
                      <Avatar.Image
                        size={30}
                        source={require("../assets/images/Avatar.png")}
                      />
                    </View>
                    <View style={{ width: "65%" }}>
                      <View>
                        <Text>{item.Name}</Text>
                      </View>
                      <View style={{ flexDirection: "row", marginTop: 5 }}>
                        <View
                          style={{
                            flexDirection: "row",
                            alignItems: "center",
                            marginRight: 5,
                          }}
                        >
                          <FontAwesome5
                            name="calendar-alt"
                            size={12}
                            color="#3BBFFC"
                          />
                          <Text style={{ color: "#3BBFFC", fontSize: 10 }}>
                            Since {item.Since}
                          </Text>
                        </View>
                        <View
                          style={{ flexDirection: "row", alignItems: "center" }}
                        >
                          <FontAwesome
                            name="building"
                            size={12}
                            color="#C54A00"
                          />
                          <Text style={{ color: "#C54A00", fontSize: 10 }}>
                            Apartment # {item.Apartment}
                          </Text>
                        </View>
                      </View>
                    </View>
                    <Entypo
                      name="dots-three-vertical"
                      size={14}
                      color="black"
                    />
                  </View>
                </Card>
              </View>
            ))}
        </ScrollView>
      </Card>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  topBoxesContainer: {
    marginBottom: 30,
  },
  topBox: {
    backgroundColor: "#3BBFFC",
    paddingVertical: 15,
    borderRadius: 20,
    width: "30%",
    alignItems: "center",
    marginHorizontal: 10,
  },
  topBoxImage: {
    height: 50,
    width: 50,
    resizeMode: "contain",
    marginBottom: 10,
  },
  topBoxText: {
    color: "white",
    fontSize: 15,
    textAlign: "center",
    marginHorizontal: 20,
  },
  ListInnerContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
});
