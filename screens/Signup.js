import React, { useState, useRef } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TextInput,
  Keyboard,
  TouchableWithoutFeedback,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import {
  MaterialIcons,
  EvilIcons,
  AntDesign,
  FontAwesome,
} from "@expo/vector-icons";
import firebase from "../server/db/firestore";
import { FirebaseRecaptchaVerifierModal } from "expo-firebase-recaptcha";

export default function Signup({ navigation }) {
  const [enteredEmail, setEnteredEmail] = useState("");
  const [enteredPassword, setEnteredPassword] = useState("");
  const [reEnteredPassword, setReEnteredPassword] = useState("");
  const [enteredPhone, setEnteredPhone] = useState("");
  const [incomplete, setIncomplete] = useState(false);
  const [mismatch, setMismatch] = useState(false);
  const recaptchaVerifier = useRef(null);
  const [verificationId, setVerificationId] = useState(null);

  const SignUpButtonHandler = () => {
    if (
      enteredEmail == "" ||
      enteredPassword == "" ||
      reEnteredPassword == "" ||
      enteredPhone == ""
    ) {
      setMismatch(false);
      setIncomplete(true);
    } else if (reEnteredPassword != enteredPassword) {
      setMismatch(true);
      setIncomplete(false);
    } else {
      setIncomplete(false);
      setMismatch(false);
      const phoneProvider = new firebase.auth.PhoneAuthProvider();
      phoneProvider
        .verifyPhoneNumber(enteredPhone, recaptchaVerifier.current)
        .then(setVerificationId);
      // navigation.navigate("PhoneVerification", {
      //   verificationId: verificationId,
      // });
    }
    // db.collection("users")
    //   .add({
    //     Email: enteredEmail,
    //     Password: enteredPassword,
    //   })
    //   .then((result) => navigation.replace("PhoneVerification"))
    //   .catch((err) => console.log(err));
    //navigation.navigate("PhoneVerification");
  };

  React.useEffect(() => {
    if (verificationId != null) {
      navigation.navigate("PhoneVerification", {
        verificationId: verificationId,
      });
    }
  }, [verificationId]);

  const EmailHandler = (enteredText) => {
    setEnteredEmail(enteredText);
  };
  const PasswordHandler = (enteredText) => {
    setEnteredPassword(enteredText);
  };
  const ConfirmPasswordHandler = (enteredText) => {
    setReEnteredPassword(enteredText);
  };
  const PhoneHandler = (enteredText) => {
    setEnteredPhone(enteredText);
  };
  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <View style={styles.container}>
        <View style={styles.logoContainer}>
          <Image
            source={require("../assets/images/app-Coloured-Logo.png")}
            style={{ height: 100, width: "50%" }}
          />
        </View>
        <FirebaseRecaptchaVerifierModal
          ref={recaptchaVerifier}
          firebaseConfig={firebase.app().options}
          attemptInvisibleVerification={true}
        />
        <ScrollView showsVerticalScrollIndicator={false}>
          <TouchableWithoutFeedback>
            <View style={styles.LoginFormContainer}>
              <Text style={styles.LoginHeading}>Sign Up</Text>
              <Text style={styles.detailsText}>
                Please enter your email & phone number to Sign Up
              </Text>
              {incomplete ? (
                <Text style={styles.errorText}>
                  Please enter all of the information in the given fields
                </Text>
              ) : null}
              {mismatch ? (
                <Text style={styles.errorText}>
                  Password and re-Entered Password do not match
                </Text>
              ) : null}
              <View style={styles.inputFieldContainer}>
                <MaterialIcons name="email" size={24} color="white" />
                <TextInput
                  placeholder="Email"
                  placeholderTextColor="white"
                  style={styles.inputStyle}
                  onChangeText={EmailHandler}
                />
              </View>
              <View style={styles.inputFieldContainer}>
                <FontAwesome name="phone" size={24} color="white" />
                <TextInput
                  placeholder="Phone"
                  placeholderTextColor="white"
                  style={styles.inputStyle}
                  onChangeText={PhoneHandler}
                  keyboardType="phone-pad"
                />
              </View>
              <View style={styles.inputFieldContainer}>
                <EvilIcons name="unlock" size={28} color="white" />
                <TextInput
                  placeholder="Password"
                  placeholderTextColor="white"
                  style={styles.inputStyle}
                  secureTextEntry={true}
                  onChangeText={PasswordHandler}
                />
              </View>
              <View style={styles.inputFieldContainer}>
                <EvilIcons name="unlock" size={28} color="white" />
                <TextInput
                  placeholder="Re-Enter Password"
                  placeholderTextColor="white"
                  style={styles.inputStyle}
                  secureTextEntry={true}
                  onChangeText={ConfirmPasswordHandler}
                />
              </View>
              <TouchableOpacity onPress={SignUpButtonHandler}>
                <View style={styles.SignupButton}>
                  <Text style={{ color: "white", textAlign: "center" }}>
                    Sign Up
                  </Text>
                </View>
              </TouchableOpacity>
              <Text
                style={{
                  color: "white",
                  textAlign: "center",
                  marginBottom: 20,
                }}
              >
                OR
              </Text>
              <View style={styles.SocialButtons}>
                <TouchableOpacity
                  style={styles.GoogleButton}
                  activeOpacity={0.8}
                >
                  <AntDesign
                    name="google"
                    size={20}
                    color="red"
                    style={{ marginLeft: 10 }}
                  />
                  <Text style={{ marginLeft: 10 }}>Google</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.FacebookButton}
                  activeOpacity={0.8}
                >
                  <FontAwesome
                    name="facebook"
                    size={24}
                    color="white"
                    style={{ marginLeft: 10 }}
                  />
                  <Text style={{ marginLeft: 10, color: "white" }}>
                    Facebook
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </ScrollView>
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  logoContainer: {
    marginVertical: "5%",
    alignItems: "center",
  },
  LoginFormContainer: {
    backgroundColor: "#000000",
    width: "90%",
    marginHorizontal: "5%",
    paddingVertical: "15%",
    borderRadius: 25,
    marginBottom: 50,
  },
  LoginHeading: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
    fontSize: 30,
    marginBottom: 15,
  },
  detailsText: {
    color: "#BABABA",
    textAlign: "center",
    marginHorizontal: "10%",
    marginBottom: "10%",
  },
  errorText: {
    color: "red",
    textAlign: "center",
    marginHorizontal: "10%",
    marginBottom: "5%",
  },
  inputFieldContainer: {
    marginHorizontal: "10%",
    flexDirection: "row",
    paddingVertical: 5,
    borderBottomWidth: 1,
    borderBottomColor: "#BABABA",
    marginVertical: 20,
  },
  inputStyle: {
    width: "90%",
    marginLeft: 5,
    color: "white",
  },
  ForgotPasswordContainer: {
    marginHorizontal: "10%",
    flexDirection: "row",
    justifyContent: "flex-end",
    marginBottom: 20,
  },
  SignupButton: {
    backgroundColor: "#FB6E19",
    marginHorizontal: "10%",
    paddingVertical: 15,
    borderRadius: 10,
    marginBottom: 20,
  },
  GoogleButton: {
    backgroundColor: "white",
    width: "45%",
    padding: 10,
    borderRadius: 25,
    flexDirection: "row",
    alignItems: "center",
  },
  FacebookButton: {
    backgroundColor: "#245A9C",
    width: "45%",
    padding: 10,
    borderRadius: 25,
    flexDirection: "row",
    alignItems: "center",
    marginLeft: "10%",
  },
  SocialButtons: {
    marginHorizontal: "10%",
    flexDirection: "row",
  },
});
