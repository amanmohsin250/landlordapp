import React, { useState } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";

export default function TopTabs(props) {
  const [activeTab, setActiveTab] = useState(props.first);
  return (
    <View style={styles.TabsContainer}>
      {activeTab == props.first ? (
        <TouchableOpacity
          style={styles.cardButton}
          onPress={() => setActiveTab(props.first)}
        >
          <View>
            <Text style={styles.TabText}>{props.first}</Text>
          </View>
        </TouchableOpacity>
      ) : (
        <TouchableOpacity
          style={styles.button}
          onPress={() => setActiveTab(props.first)}
        >
          <View>
            <Text style={styles.TabText}>{props.first}</Text>
          </View>
        </TouchableOpacity>
      )}

      {activeTab == props.second ? (
        <TouchableOpacity
          style={styles.cardButton}
          onPress={() => setActiveTab(props.second)}
        >
          <View>
            <Text style={styles.TabText}>{props.second}</Text>
          </View>
        </TouchableOpacity>
      ) : (
        <TouchableOpacity
          style={styles.button}
          onPress={() => setActiveTab(props.second)}
        >
          <View>
            <Text style={styles.TabText}>{props.second}</Text>
          </View>
        </TouchableOpacity>
      )}

      {activeTab == props.third ? (
        <TouchableOpacity
          style={styles.cardButton}
          onPress={() => setActiveTab(props.third)}
        >
          <View>
            <Text style={styles.TabText}>{props.third}</Text>
          </View>
        </TouchableOpacity>
      ) : (
        <TouchableOpacity
          style={styles.button}
          onPress={() => setActiveTab(props.third)}
        >
          <View>
            <Text style={styles.TabText}>{props.third}</Text>
          </View>
        </TouchableOpacity>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  button: {
    backgroundColor: "#FAFBFD",
    padding: 10,
    width: "32%",
  },
  TabsContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 30,
  },
  TabText: {
    textAlign: "center",
    color: "#23478A",
  },
  cardButton: {
    borderRadius: 10,
    elevation: 3,
    backgroundColor: "#fff",
    shadowOffset: { width: 1, height: 1 },
    shadowColor: "#333",
    shadowOpacity: 0.3,
    shadowRadius: 2,
    padding: 10,
    width: "32%",
  },
});
