import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import CircularCard from "./CircularCard";
import { Ionicons } from "@expo/vector-icons";

export default function PageHeading(props) {
  return (
    <View>
      <View style={styles.headerContainer}>
        <View style={styles.headingTitle}>
          <Text style={{ fontSize: 20, fontWeight: "bold" }}>
            {props.title}
          </Text>
        </View>
        <View>
          <TouchableOpacity>
            <CircularCard>
              <Ionicons name="notifications-outline" size={24} color="black" />
            </CircularCard>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  headerContainer: {
    marginTop: "15%",
    marginHorizontal: "10%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 30,
  },
  headingTitle: {
    paddingVertical: 10,
    borderBottomWidth: 2,
    borderBottomColor: "#005FF2",
  },
});
