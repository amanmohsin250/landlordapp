import * as firebase from "firebase";

import "firebase/firestore";
import "@firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyBGzuFB-Mtc4J9Gd4ZhPXbYdIvveB13XvM",
  authDomain: "landlord-app-bf9e0.firebaseapp.com",
  projectId: "landlord-app-bf9e0",
  storageBucket: "landlord-app-bf9e0.appspot.com",
  messagingSenderId: "995115033425",
  appId: "1:995115033425:web:f60c872630915c0971a970",
};

firebase.initializeApp(firebaseConfig);

export const db = firebase.firestore();

export default firebase;
